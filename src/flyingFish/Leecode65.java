package flyingFish;

import java.util.Scanner;

public class Leecode65 {
    /**
     * 一个机器人位于一个 m x n 网格的左上角 （起始点在下图中标记为 “Start” ）。
     * <p>
     * 机器人每次只能向下或者向右移动一步。机器人试图达到网格的右下角（在下图中标记为 “Finish” ）。
     * <p>
     * 问总共有多少条不同的路径？
     */
    public static void main(String[] args) {
        System.out.println("请输入第一数字，m = ");
        Scanner input = new Scanner(System.in);
        int m = input.nextInt();
        System.out.println("请输入第二数字，n = ");
        int n = input.nextInt();
        System.out.println("递归算法结果" + getPath(m - 1, n - 1));
        int[][] dp = new int[m][n];
        getPath(m, n, dp);
        System.out.println("动态规划结果为" + dp[m - 1][n - 1]);
    }

    /**
     * 方法1：递归算法
     * @param m
     * @param n
     * @return
     */
    private static int getPath(int m, int n) {
        if (m < 0 || n < 0) return 0;
        if (m == 0) return 1;
        if (n == 0) return 1;
        return getPath(m - 1, n) + getPath(m, n - 1);
    }

    /**
     * 方法2：动态规划 dp[i][j] = dp[i][j - 1] + dp[i - 1][j]; 状态转移方程
     * @param m
     * @param n
     * @param dp
     */
    private static void getPath(int m, int n, int[][] dp) {
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (i == 0 || j == 0) {
                    dp[i][j] = 1;
                } else {
                    dp[i][j] = dp[i][j - 1] + dp[i - 1][j];
                }
            }
        }
    }
}

