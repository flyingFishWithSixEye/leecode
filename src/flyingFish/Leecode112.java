package flyingFish;

import flyingFish.entity.TreeNode;

/**
 * 给你二叉树的根节点 root 和一个表示目标和的整数 targetSum 。判断该树中是否存在 根节点到叶子节点 的路径，这条路径上所有节点值相加等于目标和 targetSum 。如果存在，返回 true ；否则，返回 false 。
 * 深度遍历---前序遍历问题
 */

public class Leecode112 {
    public boolean hasPathSum(TreeNode root, int targetSum) {

        //就是一个前序遍历问题
        if (root == null) {
            return false;
        }
        Boolean[] flag = new Boolean[]{false};
        this.getFrontWrite(root, 0, targetSum, flag);
        return flag[0];
    }

    public void getFrontWrite(TreeNode root, int sum, int targetSum, Boolean[] flag) {
        sum += root.val;
        if (sum == targetSum && root.left == null && root.right == null) {
            flag[0] = true;
            return;
        }
        if (root.left != null) getFrontWrite(root.left, sum, targetSum, flag);
        if (root.right != null) getFrontWrite(root.right, sum, targetSum, flag);
    }

    public static void main(String[] args) {
        //[5,4,8,11,null,13,4,7,2,null,null,null,1]
        Leecode111 leecode111 = new Leecode111();
        TreeNode treeNode1 = new TreeNode(15);
        TreeNode treeNode2 = new TreeNode(7);
        TreeNode treeNode3 = new TreeNode(9);
        TreeNode treeNode4 = new TreeNode(20, treeNode1, treeNode2);
        TreeNode root = new TreeNode(3, treeNode3, treeNode4);
        Leecode112 leecode112 = new Leecode112();
        System.out.println(leecode112.hasPathSum(root, 15));
    }
}
