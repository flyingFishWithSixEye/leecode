package flyingFish;

import flyingFish.entity.TreeNode;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 给定一个二叉树，找出其最小深度。
 * <p>
 * 最小深度是从根节点到最近叶子节点的最短路径上的节点数量。
 * <p>
 * 说明：叶子节点是指没有子节点的节点。
 *
 *
 * 广度优先算法
 */

public class Leecode111 {
    public int minDepth(TreeNode root) {
        Queue<TreeNode> queue = new LinkedList<>();
        if (root == null) {
            return 0;
        }
        root.val = 1;
        queue.add(root);
        return getDeep(queue);
    }

    public int getDeep(Queue<TreeNode> queue) {
        while (!queue.isEmpty()) {
            TreeNode treeNode = queue.poll();
            boolean flag = true;
            int length = treeNode.val;
            if (treeNode.left != null) {
                treeNode.left.val = length + 1;
                queue.add(treeNode.left);
                flag = false;
            }
            if (treeNode.right != null) {
                treeNode.right.val = length + 1;
                queue.add(treeNode.right);
                flag = false;
            }
            if (flag) return length;
        }
        return 0;
    }

    public static void main(String[] args) {
        Leecode111 leecode111 = new Leecode111();
        TreeNode treeNode1 = new TreeNode(15);
        TreeNode treeNode2 = new TreeNode(7);
        TreeNode treeNode3 = new TreeNode(9);
        TreeNode treeNode4 = new TreeNode(20,treeNode1,treeNode2);
        TreeNode root = new TreeNode(3,treeNode3,treeNode4);
        int i = leecode111.minDepth(root);
        System.out.println(i);
    }
}


