package flyingFish;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Leecode5 {
    /**
     * 给你一个字符串 s，找到 s 中最长的回文子串。
     * <p>
     * 示例 1：
     * <p>
     * 输入：s = "babad"
     * 输出："bab"
     * 解释："aba" 同样是符合题意的答案。
     * 示例 2：
     * <p>
     * 输入：s = "cbbd"
     * 输出："bb"
     * 示例 3：
     * <p>
     * 输入：s = "a"
     * 输出："a"
     * 示例 4：
     * <p>
     * 输入：s = "ac"
     * 输出："a"
     */
    public static void main(String[] args) {
        //思路 双指针

        //哈哈，居然超出测试时间了，玩不玩
        Scanner input = new Scanner(System.in);
        String msg = input.nextLine();
        System.out.println(msg);
        Map<Integer, String> msgMap = new HashMap<>();
        if (msg != null) {
            for (int i = 0; i < msg.length(); i++) {
                msgMap.put(i, msg.substring(i, i + 1));
            }
            System.out.println(callBack(msg, msgMap));
        }
    }

    public static String callBack(String msg, Map<Integer, String> msgMap) {
        String result = "";
        for (int i = 0; i < msg.length(); i++) {
            if (getFold(msg, i, msgMap).length() > result.length()) {
                result = getFold(msg, i, msgMap);
            }
        }
        return result;
    }


    public static String getFold(String msg, int start, Map<Integer, String> msgMap) {
        if ("".equals(msg) || msg == null) {
            return "";
        }
        boolean flag = false;
        for (int i = msg.length() - 1; i > start; i--) {
            int endPoint = i;
            int startPoint = start;
            while (msgMap.get(endPoint).equals(msgMap.get(startPoint))) {
                if (endPoint <= startPoint) {
                    flag = true;
                    break;
                }
                endPoint--;
                startPoint++;
            }
            if (flag) {
                return msg.substring(start, i + 1);
            }
        }
        return msg.substring(start, start + 1);
    }
}

