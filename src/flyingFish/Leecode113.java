package flyingFish;

import flyingFish.entity.TreeNode;

import java.util.ArrayList;
import java.util.List;

/**
 * 给你二叉树的根节点 root 和一个整数目标和 targetSum ，找出所有 从根节点到叶子节点 路径总和等于给定目标和的路径。
 * <p>
 * 叶子节点 是指没有子节点的节点。
 */

public class Leecode113 {
    public List<List<Integer>> pathSum(TreeNode root, int targetSum) {
        List<Integer> temp = new ArrayList<>();
        List<List<Integer>> result = new ArrayList<>();
        //就是一个前序遍历问题
        if (root == null) {
            return result;
        }
        this.getFrontWrite(root, 0, targetSum, temp,result);
        return result;
    }

    public void getFrontWrite(TreeNode root, int sum, int targetSum, List<Integer> temp, List<List<Integer>> result) {
        temp.add(root.val);
        sum += root.val;
        if (sum == targetSum && root.left == null && root.right == null) {
            result.add(temp);
        }
        List<Integer> leftList = new ArrayList<>(temp);
        List<Integer> rightList = new ArrayList<>(temp);
        if (root.left != null) getFrontWrite(root.left, sum, targetSum, leftList, result);
        if (root.right != null) getFrontWrite(root.right, sum, targetSum, rightList, result);
    }

}
