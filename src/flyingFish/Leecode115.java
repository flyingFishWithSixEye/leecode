package flyingFish;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 给定一个字符串 s 和一个字符串 t ，计算在 s 的子序列中 t 出现的个数。
 * <p>
 * 字符串的一个 子序列 是指，通过删除一些（也可以不删除）字符且不干扰剩余字符相对位置所组成的新字符串。（例如，"ACE" 是 "ABCDE" 的一个子序列，而 "AEC" 不是）
 * <p>
 * 题目数据保证答案符合 32 位带符号整数范围。
 */
public class Leecode115 {
    public int numDistinct(String s, String t) {
        int numDistinct = 0;
        Map<Character, List<Integer>> total = new HashMap<>();
        for (int i = 0; i < s.length(); i++) {
            List<Integer> list = total.getOrDefault(s.charAt(i), new ArrayList<>());
            list.add(i);
            total.put(s.charAt(i), list);
        }
        List<List<Integer>> result = new ArrayList<>();
        for (int i = 0; i < t.length(); i++) {
            result.add(total.getOrDefault(t.charAt(i), new ArrayList<>()));
        }
        int[][] dp = new int[s.length()][s.length()];
        if (result.size() > 0) {
            List<Integer> list = null;
            List<Integer> preList = null;

            for (int i = 0; i < result.size(); i++) {
                list = result.get(i);
                if (i > 0) {
                    preList = result.get(i - 1);
                }
                if (list == null || list.size() <= 0) {
                    return 0;
                }
                for (int j = 0; j < list.size(); j++) {
                    if (i == 0) {
                        dp[0][j] = 1;
                    } else {
                        dp[i][j] = 0;
                        int count = list.get(j);
                        for (int k = 0; k < preList.size(); k++) {
                            if (preList.get(k) < count) dp[i][j] += dp[i - 1][k];
                        }
                    }
                    if (i == result.size() - 1) numDistinct += dp[i][j];
                }
            }
        }
        return numDistinct;
    }

    /**
     * 官方动态规划
     * @param s
     * @param t
     * @return
     */
    public int numDistinct1(String s, String t) {
        int m = s.length(), n = t.length();
        if (m < n) {
            return 0;
        }
        int[][] dp = new int[m + 1][n + 1];
        for (int i = 0; i <= m; i++) {
            dp[i][n] = 1;
        }
        for (int i = m - 1; i >= 0; i--) {
            char sChar = s.charAt(i);
            for (int j = n - 1; j >= 0; j--) {
                char tChar = t.charAt(j);
                if (sChar == tChar) {
                    dp[i][j] = dp[i + 1][j + 1] + dp[i + 1][j];
                } else {
                    dp[i][j] = dp[i + 1][j];
                }
            }
        }
        return dp[0][0];
    }


    public static void main(String[] args) {
        String s = "babgbag";
        String t = "bag";
        Leecode115 leecode115 = new Leecode115();
        System.out.println(leecode115.numDistinct1(s, t));
    }
}
