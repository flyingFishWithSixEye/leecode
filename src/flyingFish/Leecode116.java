package flyingFish;

import flyingFish.entity.Node;

import java.util.LinkedList;
import java.util.Queue;

/**
 * 给定一个 完美二叉树 ，其所有叶子节点都在同一层，每个父节点都有两个子节点。二叉树定义如下：
 * <p>
 * struct Node {
 * int val;
 * Node *left;
 * Node *right;
 * Node *next;
 * }
 * 填充它的每个 next 指针，让这个指针指向其下一个右侧节点。如果找不到下一个右侧节点，则将 next 指针设置为 NULL。
 * <p>
 * 初始状态下，所有 next 指针都被设置为 NULL。
 * <p>
 *  
 * <p>
 * 进阶：
 * <p>
 * 你只能使用常量级额外空间。
 * 使用递归解题也符合要求，本题中递归程序占用的栈空间不算做额外的空间复杂度。
 */
public class Leecode116 {
    Node tmp = null;

    public Node connect(Node root) {
        Queue<Node> queue = new LinkedList<>();
        queue.offer(root);
        while (!queue.isEmpty()) {
            int size = queue.size();
            for (int i = 0; i < size; i++) {
                tmp = queue.poll();
                if (i < size - 1) {
                    tmp.next = queue.peek();
                }
                if (tmp.left != null) queue.offer(tmp.left);
                if (tmp.right != null) queue.offer(tmp.right);
            }
        }
        return root;
    }

    public static void main(String[] args) {
        //root = [1,2,3,4,5,6,7]
        Node node = new Node(7);
        Node node1 = new Node(6);
        Node node2 = new Node(5);
        Node node3 = new Node(4);
        Node node4 = new Node(3, node1, node);
        Node node5 = new Node(2, node3, node2);
        Node node6 = new Node(1, node5, node4);

        Leecode116 leecode116 = new Leecode116();
        leecode116.connect(node6);
//        System.out.println(node6);
    }
}
