package flyingFish;

import flyingFish.entity.TreeNode;

import java.util.Stack;

/**
 * 给你二叉树的根结点 root ，请你将它展开为一个单链表：
 * <p>
 * 展开后的单链表应该同样使用 TreeNode ，其中 right 子指针指向链表中下一个结点，而左子指针始终为 null 。
 * 展开后的单链表应该与二叉树 先序遍历 顺序相同。
 */
public class Leecode114 {
    public void flatten(TreeNode root) {
        Stack<TreeNode> stack = new Stack<>();
        if (root == null) {
            return;
        }
        this.getFront(root, stack);

    }

    public void getFront(TreeNode root, Stack<TreeNode> stack) {
        if (root.right != null) {
            stack.push(root.right);
        }
        if (root.left != null) {
            stack.push(root.left);
            root.left = null;
        }
        if (!stack.empty()) {
            TreeNode pop = stack.pop();
            root.right = pop;
            getFront(pop, stack);
        }

    }

    public static void main(String[] args) {
        Leecode114 leecode114 = new Leecode114();
        TreeNode treeNode1 = new TreeNode(15);
        TreeNode treeNode2 = new TreeNode(7);
        TreeNode treeNode3 = new TreeNode(9);
        TreeNode treeNode4 = new TreeNode(20, treeNode1, treeNode2);
        TreeNode root = new TreeNode(3, treeNode3, treeNode4);
        leecode114.flatten(root);
        System.out.println(root);
    }
}
